 
package sv.com.appalumno.controller;

import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import sv.com.alumnoapp.entities.Alumno;
import sv.com.alumnoapp.model.AlumnoFacade;

 
@ManagedBean
@SessionScoped
public class AlumnoController {
    @EJB
    private AlumnoFacade alumnoFacade;
    private Alumno alumno; 
    /**
     * Creates a new instance of AlumnoController
     */
    public AlumnoController() {
        this.alumno = new Alumno();
    }
    
    //get alumno
    public Alumno getAlumno()
    {
        return alumno;
    }
    
    //set alumno
    public void setAlumno(Alumno alumno)
    {
        this.alumno = alumno;
    }
    
    //agregando alumno
    public String addAlumno()
    {
        this.alumnoFacade.create(alumno);
        this.alumno = new Alumno();
        return "index";
    }
    
    //editar alumno
    public String prepareEditAlumno(Alumno a)
    {
        this.alumno = a;
        return "edit";//vista  jsp
    }
    
    //editar alumno
    public String editAlumno()
    {
        this.alumnoFacade.edit(alumno);
        this.alumno = new Alumno();
        return "index";
    }
    
    //eliminar alumno
    public void deleteAlumno(Alumno a)
    {
        this.alumnoFacade.remove(a);
         
    }
    
    //mostrar alumnos
    public List<Alumno> listaTodoAlumnos()
    {
        return alumnoFacade.findAll();    
    }

    
    
    
}
